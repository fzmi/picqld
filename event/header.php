<?php
$activePage = basename($_SERVER['SCRIPT_FILENAME'], '.php');
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Event - <?php echo $pageTitle; ?></title>
    <link rel="icon" href="../images/icons/website-icon.png">
    <link href="../css/leaflet.css" rel="stylesheet">
    <link href="../css/style.css" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Play&display=swap" rel="stylesheet">
</head>
<body id="event-<?php echo $activePage; ?>" class="event">
    <section class="loading-screen"></section>
